#change nginx file
FROM nginx:1.16.0-alpine

RUN rm /etc/nginx/conf.d/default.conf
COPY nginx/nginx.conf /etc/nginx/conf.d

FROM node:10.17.0 as base
WORKDIR /app
COPY . /app/
RUN npm install
RUN npm install pm2 -g
RUN npm run build
RUN npm run prod



