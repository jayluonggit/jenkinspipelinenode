//importing node framework
import express from 'express'
import bodyParser from 'body-parser';
import cors from 'cors';
 
var app = express();
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

//Respond with "hello world" for requests that hit our root "/"
app.get('/', function (req, res) {
 res.send("hello jenkins node");
});

//listen to port 3000 by default
let port = 3000;
app.listen(port, ()=> {
    console.log ("Web server is realtime running on " + port );
})
 
module.exports = app;